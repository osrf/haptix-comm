# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/haptix-comm

## Issues and pull requests are backed up at

https://osrf-migration.github.io/haptix-gh-pages/#!/osrf/haptix-comm

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/haptix-comm

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

